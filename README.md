# README #

* Quick summary
>**DAPHNI** - **D**ata **A**ssociation **P**atients **H**eali**N**g **I**nfrastructure is a project for implementing proof of concept system for advance therapy using patient DNA & RNA sequencing data.

* Version
0.0.0


### Set up ###

* set up: eclipse neon, java 8, maven 3
* Configuration: none
* Dependencies: log4j1.2.17, junit4.11
* Database configuration: none
* How to run tests: junit execution or maven test
* Deployment instructions
none
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin - itai.beno@mssm.edu
* Other community or team contact