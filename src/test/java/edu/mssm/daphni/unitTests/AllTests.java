package edu.mssm.daphni.unitTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
   ResourceTest.class,
})
public class AllTests {
	
}  	
