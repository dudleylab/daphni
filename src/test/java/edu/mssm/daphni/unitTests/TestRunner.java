package edu.mssm.daphni.unitTests;

import org.apache.log4j.Logger;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
	private final static Logger m_oLogger = Logger.getLogger(TestRunner.class.getName());
	
	public static void main(String[] args) {
	      Result result = JUnitCore.runClasses(AllTests.class);
	      for (Failure failure : result.getFailures()) {
	    	  m_oLogger.error(failure.toString());
	      }
	      m_oLogger.info(result.wasSuccessful());
	   }
}
