package edu.mssm.daphni.unitTests;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.mssm.daphni.infratstructure.flows.SequenceFlow;
import edu.mssm.daphni.infratstructure.flows.dna.BWAFlow;
import edu.mssm.daphni.infratstructure.flows.dna.SamToolsIndex;
import edu.mssm.daphni.infratstructure.flows.dna.SamToolsSort;
import edu.mssm.daphni.infratstructure.flows.dna.SamToolsViewFlow;
import edu.mssm.daphni.infratstructure.flows.mutation.MutectFlow;
import edu.mssm.daphni.infratstructure.resources.Resource;
import junit.framework.TestCase;

public class ResourceTest extends TestCase {
	protected final Logger m_oLogger = Logger.getLogger(this.getClass());	    

	@Before
		protected void setUp() {
	        
	     }
	 
	@Test
	public void testExecuteFlowCommand() throws Exception {
		SequenceFlow l_oSequenceFlow = new SequenceFlow();
		Resource l_oResource=new Resource("127.0.0.1");
		l_oResource.setDryRun(true);
		
		/**
		 * raw dna data processig
		 */
		
		
		String l_sSamplename="proj_2408725";
		String[] l_lsFilesInput=new String[]{"/data1/users/benoitai/2408725_1.fastq","/data1/users/benoitai/2408725_2.fastq"};
		String l_sRefGenome="/data1/datasets/ngs/human_g1k_v37/human_g1k_v37.fasta";
		String l_sFileOutput="/data1/users/benoitai/proj_2408725.BFLOW.sam";
		
		BWAFlow l_oFlow1 =  new BWAFlow(l_sSamplename, l_lsFilesInput, l_sRefGenome, l_sFileOutput,l_oResource);
		SamToolsViewFlow l_oFlow2 = new SamToolsViewFlow(l_oFlow1.getFileOutput(),l_oFlow1.getFileOutput()+".bam", l_oResource);	
		SamToolsSort l_oFlow3 = new SamToolsSort(l_oFlow2.getFileOutput(),l_oFlow2.getFileOutput()+".sorted", l_oResource);	
		SamToolsIndex l_oFlow4 =  new SamToolsIndex(l_oFlow3.getFileOutput()+".bam",l_oResource);
		
		
		l_oSequenceFlow.addFlow(l_oFlow1);
		l_oSequenceFlow.addFlow(l_oFlow2);
		//l_oSequenceFlow.addFlow(new DeleteFileFlow(l_oFlow1.getFileOutput(),l_oResource));		
		l_oSequenceFlow.addFlow(l_oFlow3);
		l_oSequenceFlow.addFlow(l_oFlow4);
		
		
		/**
		 * Mutect flow processing
		 */
		String l_sRefGenomeMutect="/data1/users/benoitai/human_g1k_v37/human_g1k_v37.fasta";
		String l_sCosmicMutect="/data1/users/benoitai/human_g1k_v37/b37_cosmic_v54_120711.vcf";
		String l_sDSNBPMutect ="/data1/users/benoitai/human_g1k_v37/dbsnp_138.b37.vcf";
		String l_sFileInputNormalMutect="/data1/users/benoitai/proj_GRN2132514.sorted.bam";
		String l_sFileInputTumorMutect="/data1/users/benoitai/proj_11122015-LG.sorted.bam";
		String l_sFileOutputTxtMutect="/data1/users/benoitai/proj_11122015-LG.call_stats.txt";
		String l_sFileOutputCoverageMutect="/data1/users/benoitai/proj_11122015-LG.coverage.wig.txt"; 
		String l_sFileOutputVCFMutect ="/data1/users/benoitai/proj_11122015-LG.call_stats.vcf";
		
		MutectFlow l_oFlow5= new MutectFlow(l_sRefGenomeMutect,l_sCosmicMutect,l_sDSNBPMutect,
				l_sFileInputNormalMutect,l_sFileInputTumorMutect,
				l_sFileOutputTxtMutect,l_sFileOutputCoverageMutect,l_sFileOutputVCFMutect,
				l_oResource);
		
		l_oSequenceFlow.addFlow(l_oFlow5);
	
		l_oSequenceFlow.setIndex(0);
        l_oSequenceFlow.buildFlow();
		l_oSequenceFlow.execute();
		
	        l_oResource.printCommands();
	        m_oLogger.info("FINISH");
	    }
	
	@After
	public void tearDown() {
		
	}
	
	
}
