package edu.mssm.daphni.infratstructure.resources;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.mssm.daphni.infratstructure.flows.CommandFlow;

/**
 * @author Itai Beno
 *  This class handles all commands supported by different Flows to the Resource using native process.
 */
public class Resource implements IResource {
	private Boolean m_bDryRun;
	private InetAddress m_oIP;
	private Process m_oProcess;
	protected final Logger m_oLogger = Logger.getLogger(this.getClass());
	private static final String BASH_FIRST_LINE = "#!/bin/bash -x";
	private ArrayList<String> m_olCommands;

	public Resource(String a_sIP) throws UnknownHostException {
		m_oLogger.info("set ip for: " + a_sIP  );
		if (m_oIP == null){
			this.m_oIP = InetAddress.getByName(a_sIP);
		}
		m_oLogger.info("ip = " + this.m_oIP  );
		m_olCommands=new ArrayList<String>();
	}
	@Override
	public boolean bConnectToResource() {

		return true;
	}

	@Override
	public boolean isAlive() {
		return true;
	}

	@Override
	public void closeConnection() {

		if (m_oProcess!=null){
			m_oProcess.destroy();
		}
	}

	/**
	 * Execute the flow command in a bash shell.
	 * @param ao_CommandFlow the flow contains the command and the process parameters
	 * @return
	 * @throws Exception
	 */
	public int runShellCommand(CommandFlow ao_CommandFlow) throws Exception
	{
		m_oLogger.info("enter runShellCommand" );
		//build bash script from Flow command and execution directory
		String ls_ScriptPath=ao_CommandFlow.getProcessDirectory();
		if(ls_ScriptPath==null){
			ls_ScriptPath=".";
		}
		String ls_Command=ao_CommandFlow.getCommand();
		String ls_ScriptName=ls_ScriptPath+ao_CommandFlow.getIndex()+".sh"; 

		createBashScript(ls_Command,ls_ScriptName);
		if(isDryRun()){
			m_oLogger.info("Dry run mode" );
			return 0;
		}


		//the command shall be executed via bash shell to simplify passing parameters to Process builder.
		ProcessBuilder pb = new ProcessBuilder(ls_ScriptName);
		//set the process working directory
		pb.directory(new File(ao_CommandFlow.getProcessDirectory()));
		//set process environment variables
		if(ao_CommandFlow.getProcessEnvironment()!=null){
			Map<String, String> env = pb.environment();
			env.putAll(ao_CommandFlow.getProcessEnvironment());
		}
		//set the source and destination for subprocess standard I/O to be the same as those of the current Java process.
		if(ao_CommandFlow.isProcessInheritIO())
		{
			pb.inheritIO();
		}

		//save process stream into file
		if(ao_CommandFlow.isProcessRedirect())
		{
			m_oLogger.info("\nRedirect error so that error is redirected to output file");
			pb.redirectErrorStream(true);
			File commonOutputFile = new File(ao_CommandFlow.getProcessCommonFile());
			pb.redirectOutput(commonOutputFile);
		}
		else{
			m_oLogger.info("Redirect output and error to different files");
			File outputFile = new File(ao_CommandFlow.getProcessOutputFile());
			File errorFile = new File(ao_CommandFlow.getProcessErrorFile());
			pb.redirectOutput(outputFile);
			pb.redirectError(errorFile);
		}

		m_oProcess = pb.start();	
		int ret=m_oProcess.waitFor();
		m_oLogger.info("exit: " + m_oProcess.exitValue());
		m_oProcess.destroy();
		return ret;
	}

	/**
	 * Write content string to a specified script file
	 * change the permission of script to +x if not in dry run mode.
	 * 
	 * @param as_Command to add to file
	 * @param as_ScriptName the output script file 
	 * @throws IOException
	 */
	private void createBashScript(String as_Command,String as_ScriptName) throws Exception {
		FileWriter fw = new FileWriter(as_ScriptName,true);
		PrintWriter pw = new PrintWriter(fw);
		pw.println(BASH_FIRST_LINE);
		pw.println(as_Command);
		m_olCommands.add(as_Command);
		pw.close();
		if(isDryRun()){
			return;
		}
		String cmd = new String("/bin/chmod +x "+as_ScriptName);
		m_oLogger.info("command="+cmd);
		Process pr = Runtime.getRuntime().exec(cmd);
		int li_ProRetVal = pr.waitFor();
		m_oLogger.info("retvalue = "+ li_ProRetVal);
		if (li_ProRetVal!=0) {
			m_oLogger.error("chmod u+x failed for script: "+as_ScriptName);
			throw new Exception("chmod +x failed for script: "+as_ScriptName);
		}

		return;
	}

	/**
	 * determine if to execute the command or just write the to log.
	 * @param m_bDryRun - true to dry run.
	 */
	public void setDryRun(Boolean m_bDryRun) {
		this.m_bDryRun = m_bDryRun;
	}
	public Boolean isDryRun() {
		return m_bDryRun;
	}
	public void printCommands() {
		m_oLogger.info("###############################################################");
		m_oLogger.info(BASH_FIRST_LINE);
		for (Iterator<String> iterator = m_olCommands.iterator(); iterator.hasNext();) {
			String ls_Commands = (String) iterator.next();
			m_oLogger.info(ls_Commands);
		}
		m_oLogger.info("###############################################################");
		
	}	
}
