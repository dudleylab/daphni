/**
 * 
 */
package edu.mssm.daphni.infratstructure.resources;

/**
 * @author Itai Beno
 * An interface that represents a resource in the Infrastructure.
 */
public interface IResource {
 boolean bConnectToResource();
 boolean isAlive();
 void closeConnection();
}
