package edu.mssm.daphni.infratstructure.flows;

import java.util.Collection;

/**
 * @author Itai Beno
 *  An interface represents a flow that can be performed on a resource.
 *  
 */

public interface IFlow {

	/**
	 * Builds the flow behavior. 
	 *
	 */
	void buildFlow();

	/**
	 * Execute the flow.
	 @return <tt>EResult.eResultCompleted<tt> upon process execution return zero
	 */
	EResult execute() throws Exception;

	/**
	 * set the index of the flow as appears in the pipeline.
	 * @param ai_Index
	 */
	public void setIndex(int ai_Index);
		

}
