package edu.mssm.daphni.infratstructure.flows.dna;

import edu.mssm.daphni.infratstructure.flows.CommandFlow;
import edu.mssm.daphni.infratstructure.flows.IFlow;
import edu.mssm.daphni.infratstructure.resources.IResource;

public class SamToolsIndex extends CommandFlow implements IFlow{
	/**
	 * specific parameters passed to the SamToolsIndex
	 */
	private String m_sFileInput;

	public SamToolsIndex(String a_sFileInput,IResource a_oResource) {
		m_oResource=a_oResource;
		m_sFileInput=a_sFileInput;
	}

	@Override
	public void buildFlow() {
		setProcessDirectory("/data1/users/benoitai/");
		setProcessInheritIO(true);
		setProcessRedirect(true);
	}

	@Override
	public String getCommand() {
		StringBuilder cmd = new StringBuilder();
		cmd.append("samtools index ");
		cmd.append(m_sFileInput);
		m_oLogger.info(cmd.toString());
		return cmd.toString(); 
	}

}