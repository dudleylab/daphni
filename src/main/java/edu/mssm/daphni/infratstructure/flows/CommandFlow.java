/**
 * 
 */
package edu.mssm.daphni.infratstructure.flows;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.mssm.daphni.infratstructure.resources.IResource;
import edu.mssm.daphni.infratstructure.resources.Resource;

/**
 * @author benoi01
 * 	base class use to execute flows based on Linux command to run on a resource.
 *
 */
public abstract class CommandFlow implements IFlow {

	private int m_iIndex;
	protected final Logger m_oLogger = Logger.getLogger(this.getClass());
	/*
	 * The resource which execute the flow command on new Resource's process.
	 */
	protected IResource m_oResource;

	/*
	 * determine if to set the source and destination for subprocess standard I/O to be the same as those of the current Java process.
	 */
	boolean m_bProcessInheritIO;
	
	/*
	 * determine if to save process stream into file
	 */
	boolean m_bProcessRedirect;
	
	/*
	 * map view of variables to set in the process environment
	 */
	Map<String, String> m_oProcessEnvironment;
	
	/*
	 * working directory for process execution  
	 */
	String m_sProcessDirectory;
	
	/*
	 * File name to save the process output stream  
	 */
	String m_sProcessOutputFile;
	
	/*
	 * File name to save the process error stream  
	 */
	String m_sProcessErrorFile;
	
	/*
	 * File name contains process's output and error stream
	 */
	String m_sProcessCommonFile;


	
	/**
     * Returns the working directory for process execution 
     * @default is null i.e. use the working directory of the current Java process.
     * 
     * @return the directory file name
     */
	 public String getProcessDirectory(){
	   return m_sProcessDirectory;	
	}
	
	/**
     * Returns a string map view of variables to set in the process environment.
     * @return the new environment variables to add to process environment.
     */
	public Map<String, String> getProcessEnvironment(){
		return m_oProcessEnvironment;
	}

	/**
	 * determine if to set the source and destination for subprocess standard I/O to be the same as those of the current Java process.
	 */
	public boolean isProcessInheritIO() {
		return m_bProcessInheritIO;
	}
	
	/**
	 * determine if to save process stream into file
	 */
	public boolean isProcessRedirect() {
		return m_bProcessRedirect;
	}
	
	
	/**
     * Returns the file name to save the process output stream  
     * @default processOutputLog.txt
     * @return the output file name
     */	
	public String getProcessOutputFile(){
		if(m_sProcessOutputFile==null){
			m_sProcessOutputFile = "processOutputLog.txt";
		}
		return m_sProcessOutputFile;
	}

	/**
     * Returns the file name to save the process error stream  
     * @default processErrorlog.txt
     * @return the error file name
     */
	public String getProcessErrorFile(){
		if(m_sProcessErrorFile==null){
			m_sProcessErrorFile = "processErrorLog.txt";
		}
		return m_sProcessErrorFile;
	}

	/**
     * Returns the file name to save the process output and error stream  
     * @default processCommonLog.txt
     * @return the file name contains process's output and error stream
     */
	public String getProcessCommonFile(){
		if(m_sProcessOutputFile==null){
			m_sProcessCommonFile = "processCommonLog.txt";
		}
		return m_sProcessCommonFile;
	}
	
	public void setProcessInheritIO(boolean m_bProcessInheritIO) {
		this.m_bProcessInheritIO = m_bProcessInheritIO;
	}

	public void setProcessRedirect(boolean m_bProcessRedirect) {
		this.m_bProcessRedirect = m_bProcessRedirect;
	}

	public void setProcessEnvironment(Map<String, String> m_oProcessEnvironment) {
		this.m_oProcessEnvironment = m_oProcessEnvironment;
	}

	public void setProcessDirectory(String m_sProcessDirectory) {
		this.m_sProcessDirectory = m_sProcessDirectory;
	}

	public void setProcessOutputFile(String m_sProcessOutputFile) {
		this.m_sProcessOutputFile = m_sProcessOutputFile;
	}

	public void setProcessErrorFile(String m_sProcessErrorFile) {
		this.m_sProcessErrorFile = m_sProcessErrorFile;
	}

	public void setProcessCommonFile(String m_sProcessCommonFile) {
		this.m_sProcessCommonFile = m_sProcessCommonFile;
	}

	/**
	 * get the index of the flow as appears in the pipeline.
	 * @return Index of the flow
	 */
	public int getIndex() {
		return m_iIndex;
	}
	/**
	 * set the index of the flow as appears in the pipeline.
	 * @param ai_Index
	 */
	public void setIndex(int ai_Index) {
		this.m_iIndex = ai_Index;
	}


	/* (non-Javadoc)
	 * @see edu.mssm.infratstructure.flows.IFlow#execute()
	 */
	@Override
	public EResult execute() throws Exception {
		int l_nProcRetVal=-1;
		try {
			l_nProcRetVal=((Resource)m_oResource).runShellCommand(this);//run the command on a resource
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			return EResult.eResultError;
		}
		if(l_nProcRetVal==0){
			return EResult.eResultCompleted;
		}
		return EResult.eResultFailed;
	}

	/* (non-Javadoc)
	 * @see edu.mssm.infratstructure.flows.IFlow#getCommand()
	 */
	
	 /**
     * Returns the command this flow execute 
     * 
     * @return the command to execute
     */
	public abstract String getCommand();
	
}
