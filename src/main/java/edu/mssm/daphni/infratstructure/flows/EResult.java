package edu.mssm.daphni.infratstructure.flows;
/**
 * @author Itai Beno
 * <p> 
 * <tt>Enum<tt> that represents result for performing IFlow execute function.
 * <p>
 * <p>
 * <tt>eResultCompleted<tt>- Flow terminate with exit zero code<br>
 * <tt>eResultFailed<tt>- Flow terminate with exit code other than zero<br> 
 * <tt>eResultError<tt>- Flow terminate by error  <br>
 * <tt>eResultFailed<tt>-<br>
 * <tt>eResultCancelled<tt>-<br>
 * <tt>eResultUnknown<tt>-<br>
  * <p>
 */
public enum EResult {
	eResultCompleted,
	eResultFailed,
	eResultError,
	eResultCancelled,
	eResultUnknown
}
