package edu.mssm.daphni.infratstructure.flows;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author Itai Beno
 * represents a sequence of flows; it has a member list of flows.
 */
public class SequenceFlow implements IFlow {

	ArrayList<IFlow> m_olFlowSequence; //holds the Flows used to build the Sequence.
	@Override
	public void buildFlow() {
		for (Iterator<IFlow> l_iterator = m_olFlowSequence.iterator(); l_iterator.hasNext();) {
			//iterate over the Sequence and manipulate the flow as desired.
			IFlow l_IFlow = (IFlow) l_iterator.next();
			l_IFlow.buildFlow();
		}
	}

	@Override
	public void setIndex(int ai_Index) {
		for (Iterator<IFlow> l_iterator = m_olFlowSequence.iterator(); l_iterator.hasNext();) {
			//iterate over the Sequence and manipulate the flow as desired.
			IFlow l_IFlow = (IFlow) l_iterator.next();
			l_IFlow.setIndex(++ai_Index);
		}
	}
	
	@Override
	//iterate over the sequence and execute each Flow construct the Sequence.
	public EResult execute() throws Exception {
		EResult l_eRes = EResult.eResultUnknown;
		for (Iterator<IFlow> l_iterator = m_olFlowSequence.iterator(); l_iterator.hasNext();) {
			IFlow iFlow = (IFlow) l_iterator.next();
			l_eRes=iFlow.execute();
			if(l_eRes!=EResult.eResultCompleted){
				throw new FlowException();
			}
		}

		return EResult.eResultCompleted;
	}

	  /**
     * Appends the specified Flow to the end of this list.
     *
     * @param a_oFlow IFlow element to be appended to the end of the list
     * @return <tt>true</tt> (as specified by {@link Collection#add})
     */
	public boolean addFlow(IFlow a_oFlow){
		return getM_olFlowSequence().add(a_oFlow);
	}

	public ArrayList<IFlow> getM_olFlowSequence() {
		if(m_olFlowSequence==null){
			m_olFlowSequence=new ArrayList<IFlow>();
		}
		return m_olFlowSequence;
	}
}
