package edu.mssm.daphni.infratstructure.flows.dna;

import edu.mssm.daphni.infratstructure.flows.CommandFlow;
import edu.mssm.daphni.infratstructure.flows.IFlow;
import edu.mssm.daphni.infratstructure.resources.IResource;

public class SamToolsSort extends CommandFlow  implements IFlow{
	/**
	 * specific parameters passed to the SamToolsSort
	 */
	private String m_sFileInput;
	private String m_sFileOutput;

	public SamToolsSort(String a_sFileInput, String a_sFileOutput,IResource a_oResource) {
		m_oResource=a_oResource;
		m_sFileInput=a_sFileInput;
		m_sFileOutput=a_sFileOutput;

	}
	@Override
	public void buildFlow() {
		setProcessDirectory("/data1/users/benoitai/");
		setProcessInheritIO(true);
		setProcessRedirect(true);
	}

	@Override
	public String getCommand() {
		StringBuilder cmd = new StringBuilder();
		cmd.append("samtools sort ");
		cmd.append(m_sFileInput);
		cmd.append(" "+m_sFileOutput);
		m_oLogger.info(cmd.toString());
		return cmd.toString(); 
	}
	public String getFileOutput() {
		return m_sFileOutput;
	}
}
