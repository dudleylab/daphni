package edu.mssm.daphni.infratstructure.flows.global;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;


public class GlobalsProperties {
	private static final Logger m_oLogger = Logger.getLogger(GlobalsProperties.class.getName());
	
	private static final String BUNDLE_NAME = "edu.mssm.daphni.infratstructure.flows.configuration.daphni.properties";

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
	        .getBundle(BUNDLE_NAME);


	private GlobalsProperties() {
	}

	public static long getLong(String key) {
	    String strValue = getString(key);
	    long result = -1;
	    try {
	        result = Long.parseLong(strValue);
	    } catch (Exception exc) {
	    	//m_oLogger.warning(exc.getLocalizedMessage());
	    }
	    return result;
	}

	public static int getInteger(String key) {
	    String strValue = getString(key);
	    int result = -1;
	    try {
	        result = Integer.parseInt(strValue);
	    } catch (Exception exc) {
	    //	m_oLogger.warning(exc.getLocalizedMessage());
	    }
	    return result;
	}

	public static String getString(String key) {
	    String returnValue = System.getProperty(key);
	    if(returnValue != null && returnValue.length() > 0) {
	        if(m_oLogger.isDebugEnabled()) {
	        	m_oLogger.debug(key+" assigned by system property");
	        }
	        return returnValue;
	    }
	    try {
	        returnValue = RESOURCE_BUNDLE.getString(key);
	    } catch (MissingResourceException e) {
	        returnValue = '!' + key + '!';
	    }
	    return returnValue;
	}

}
