package edu.mssm.daphni.infratstructure.flows.mutation;

import edu.mssm.daphni.infratstructure.flows.CommandFlow;
import edu.mssm.daphni.infratstructure.flows.IFlow;
import edu.mssm.daphni.infratstructure.resources.IResource;

public class MutectFlow extends CommandFlow implements IFlow{

	public static final String JAVA_7 = "/data1/software/jdk1.7.0_79/bin/java ";
	public static final String MUTECT_JAR = "/data1/software/mutect/mutect-1.1.7.jar ";

	/**
	 * specific parameters passed to the BWAFlow
	 */
	private String m_sRefGenome;
	private String m_sCosmic;
	private String m_sDSNBP;
	private String m_sFileInputNormal;
	private String m_sFileInputTumor;
	private String m_sFileOutputTxt;
	private String m_sFileOutputCoverage;
	private String m_sFileOutputVCF;

	public MutectFlow(String a_sRefGenome,String a_sCosmic,String a_sDSNBP,
			String a_sFileInputNormal,String a_sFileInputTumor,
			String a_sFileOutputTxt,String a_sFileOutputCoverage,String a_sFileOutputVCF,
			IResource a_oResource) {

		m_sRefGenome=a_sRefGenome;                    
		m_sCosmic=a_sCosmic;                       
		m_sDSNBP=a_sDSNBP;
		m_sFileInputNormal=a_sFileInputNormal;
		m_sFileInputTumor=a_sFileInputTumor;
		m_sFileOutputTxt=a_sFileOutputTxt;
		m_sFileOutputCoverage=a_sFileOutputCoverage;
		m_sFileOutputVCF=a_sFileOutputVCF;

		m_oResource=a_oResource;  

	}                                              
	@Override
	public void buildFlow() {
		setProcessDirectory("/data1/users/benoitai/");
		setProcessInheritIO(true);
		setProcessRedirect(true);
	}

	@Override
	public String getCommand() {
		// data1/software/jdk1.7.0_79/bin/java -Xmx2g -jar /data1/software/mutect/mutect-1.1.7.jar
		//--analysis_type MuTect --reference_sequence /data1/datasets/ngs/human_g1k_v37/human_g1k_v37.fasta 
		//--cosmic /data1/datasets/ngs/human_g1k_v37/b37_cosmic_v54_120711.vcf 
		//--dbsnp /data1/datasets/ngs/human_g1k_v37/dbsnp_138.b37.vcf 
		//--input_file:normal proj_GRN2132514.sorted.bam --input_file:tumor proj_11122015-LG.sorted.bam 
		//--out proj_11122015-LG.call_stats.txt
		//--coverage_file proj_11122015-LG.coverage.wig.txt 
		//--vcf proj_11122015-LG.call_stats.vcf
		StringBuilder cmd = new StringBuilder();
		cmd.append(JAVA_7 + "-Xmx2g -jar " + MUTECT_JAR);
		cmd.append(" --analysis_type MuTect --reference_sequence " + m_sRefGenome); 
		cmd.append(" --cosmic " + m_sCosmic); 
		cmd.append(" --dbsnp " + m_sDSNBP); 
		cmd.append(" --input_file:normal " + m_sFileInputNormal + " --input_file:tumor " + m_sFileInputTumor); 
		cmd.append(" --out " + m_sFileOutputTxt);
		cmd.append(" --coverage_file " + m_sFileOutputCoverage); 
		cmd.append(" --vcf " + m_sFileOutputVCF);
		m_oLogger.info(cmd.toString());
		return cmd.toString();	}


}
