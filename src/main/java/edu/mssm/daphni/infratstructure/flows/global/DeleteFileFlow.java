package edu.mssm.daphni.infratstructure.flows.global;

import edu.mssm.daphni.infratstructure.flows.CommandFlow;
import edu.mssm.daphni.infratstructure.flows.IFlow;
import edu.mssm.daphni.infratstructure.resources.IResource;

/**
 * The Flow is used to delete system file specified by a file path
 * @author benoitai
 *
 */
public class DeleteFileFlow extends CommandFlow implements IFlow{

	/**
	 * specific parameters passed to the Flow
	 */
	String m_sFilePath;

	public DeleteFileFlow(String a_sFilePath,IResource a_oResource) {
		m_oResource=a_oResource;
		m_sFilePath=a_sFilePath;
	}

	@Override
	public void buildFlow() {
		setProcessDirectory("/data1/users/benoitai/");
		setProcessInheritIO(true);
		setProcessRedirect(true);
	}

	@Override
	public String getCommand() {
		StringBuilder cmd = new StringBuilder();
		cmd.append("/bin/rm -f ");
		cmd.append(m_sFilePath);
		m_oLogger.info(cmd.toString());
		return cmd.toString(); 
	}

}