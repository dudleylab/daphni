package edu.mssm.daphni.infratstructure.flows.dna;

import edu.mssm.daphni.infratstructure.flows.CommandFlow;
import edu.mssm.daphni.infratstructure.flows.IFlow;
import edu.mssm.daphni.infratstructure.resources.IResource;

public class SamToolsViewFlow extends CommandFlow implements IFlow{
	/**
	 * specific parameters passed to the SemToolsViewFlow
	 */
	private String m_sFileInput;
	private String m_sFileOutput;
	
	public SamToolsViewFlow(String a_sFileInput, String a_sFileOutput,IResource a_oResource) {
		m_oResource=a_oResource;
		m_sFileInput=a_sFileInput;
		m_sFileOutput=a_sFileOutput;

	}
	public String getFileOutput() {
		return m_sFileOutput;
	}
	@Override
	public void buildFlow() {
		setProcessDirectory("/data1/users/benoitai/");
		setProcessInheritIO(true);
		setProcessRedirect(true);
	}

	@Override
	public String getCommand() {
		StringBuilder cmd = new StringBuilder();
		cmd.append("samtools view -b -S ");
			cmd.append(m_sFileInput);
		cmd.append(" > "+m_sFileOutput);
		m_oLogger.info(cmd.toString());
		return cmd.toString(); 
	}

}
