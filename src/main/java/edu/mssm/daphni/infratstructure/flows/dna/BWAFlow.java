/**
 * 
 */
package edu.mssm.daphni.infratstructure.flows.dna;

import java.io.IOException;
import java.util.Map;

import edu.mssm.daphni.infratstructure.flows.CommandFlow;
import edu.mssm.daphni.infratstructure.flows.EResult;
import edu.mssm.daphni.infratstructure.flows.FlowException;
import edu.mssm.daphni.infratstructure.flows.IFlow;
import edu.mssm.daphni.infratstructure.resources.IResource;
import edu.mssm.daphni.infratstructure.resources.Resource;

/**
 * @author Itai Beno
 *  bwa

*/

public class BWAFlow extends CommandFlow implements IFlow {
	
	/**
	 * specific parameters passed to the BWAFlow
	 */
	private String m_sSampleName;
	private String m_sFileOutput;
	private String m_sRefGenome;
	private String[] m_lsFilesInput;
	
	public BWAFlow(String a_sSampleName,String[] a_lsFilesInput,String a_sRefGenome,String a_sFileOutput,IResource a_oResource) {
		m_oResource=a_oResource;
		m_sSampleName=a_sSampleName;
		m_lsFilesInput=a_lsFilesInput;
		m_sRefGenome=a_sRefGenome;
		m_sFileOutput=a_sFileOutput;
	}
	public String getFileOutput() {
		return m_sFileOutput;
	}
	/* (non-Javadoc)
	 * @see edu.mssm.infratstructure.flows.IFlow#buildFlow()
	 */
	@Override
	public void buildFlow() {
		setProcessDirectory("/data1/users/benoitai/");
		setProcessInheritIO(true);
		setProcessRedirect(true);
	}




	/* (non-Javadoc)
	 * @see edu.mssm.infratstructure.flows.IFlow#getCommand()
	 */
	@Override
	public String getCommand() {
		StringBuilder cmd = new StringBuilder();
		cmd.append("bwa mem -M -R \"@RG\\tID:" + m_sSampleName + "\\tSM:" + m_sSampleName + "\\tLB:" + m_sSampleName + "\\tPL:" + "truseq\\tPU:-\" -v 1 -t 12 " + m_sRefGenome);
		for (int i = 0; i < m_lsFilesInput.length; i++) {
			cmd.append(" "+m_lsFilesInput[i]);
		}
		cmd.append(" > "+m_sFileOutput);	
		m_oLogger.info(cmd.toString());
		return cmd.toString();
		
	}


}
